FROM ubuntu:xenial
MAINTAINER Dale Glass <daleglass@gmail.com>

ARG jenkins_uid=983
ARG jenkins_gid=983
ARG xz_source=https://tukaani.org/xz/xz-5.2.4.tar.xz



# This is a work in progress. The final intention is to build RPM packages of HiFi.
#
# Notes:
#
# Build fails on Fedora 30 due to statx calls failing with EPERM. 
# This is a docker issue: https://github.com/moby/moby/pull/36417
# Can be worked around by:
#     docker run --security-opt seccomp=statx_fixed.json
#
# To use strace inside the container, run with:
#     docker run --cap-add SYS_PTRACE


RUN sed -r -i 's/#\s*deb-src/deb-src/g' /etc/apt/sources.list
RUN apt update
RUN apt -y -u dist-upgrade

# Dependencies as per
# https://github.com/highfidelity/hifi/blob/master/tools/qt-builder/README.md

RUN apt build-dep -y qt5-default
RUN apt install -y build-essential git python gperf flex bison pkg-config libgl1-mesa-dev make g++ libdbus-glib-1-dev libnss3-dev 


# Additional tools not required for an actual build
# xz and pigz both can compress in parallel and are useful for making tarballs.
RUN apt install -y xz-utils pigz rsync libfuse2 wget patchelf openssh-client fuse

# Optional debug tools
RUN apt install -y strace vim

### xz
# Xenial doesn't have a parallel capable xz. This is a problem for my 16 core build
# server.
WORKDIR /tmp/xz
RUN wget $xz_source
RUN tar -xvf * && rm *.tar.xz
RUN cd * && ./configure && make && make install
RUN ldconfig

# We want to build stuff as a normal user
RUN useradd -m -u 2000  hifi

# Okay, this is a really ugly hack. Here's the problem: Jenkins runs the container under its own user ID,
# which for me happens to be 983.
#
# The AppImage packager really wants to know the username for some reason, so the user account has to exist.
# So as a temporary fix I'm just creating the user here. Future solutions might involve sudo, fakeroot, the
# AppImage packager, or just mass creating accounts, I'm not sure which.

RUN groupadd -g $jenkins_gid jenkins
RUN useradd -m -u $jenkins_uid -g $jenkins_gid  jenkins

USER hifi

ENTRYPOINT /bin/bash

